
function scene_3(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/scene_2.mp3');
  audio.loop = true;
  audio.play();

  var connector = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/arms_connect.png",60,512,512,256,256,true,1.0);
  var connector_2 = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/arms_connect_2.png",20,512,512,256,256,false,0.0);

  var connect = false;

  var yeah = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/yeah.png",40,256,256,128,128,false,1.0);
  var ohno = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/ohno.png",40,256,256,128,128,false,1.0);

  function moveOn(){
    audio.pause();
    renderer.play = false;
  }

  function connectorControl(el){

    if(keys['32'] || keys["mouse"]){
      if(el.pressed == false){
        if(el.sprite_current_frame >= 0 && el.sprite_current_frame <= 20){

          el.sprite_loop = false;
          connector_2.opacity = 1.0;
          connector_2.sprite_run_once_and_pause = true;
          connector_2.sprite_run = 1;
          connect = true;
          setTimeout(moveOn,500);

        }else if(el.sprite_current_frame >= 70 && el.sprite_current_frame <= 90){

          el.sprite_loop = false;
          connector_2.opacity = 1.0;
          connector_2.sprite_run_once_and_pause = true;
          connector_2.sprite_run = 1;
          connect = true;
          setTimeout(moveOn,500);

        }
        el.pressed = true;
      }
    }else{
      el.pressed = false;
    }

  }

  function moveOn(){
    if(connect == true){
      yeah.sprite_run = 1;
    }else{
      ohno.sprite_run = 1;
    }
    function am(){
      audio.pause();
      renderer.play = false;
    }
    setTimeout(am,1000);
  }

  setTimeout(moveOn,10000);

  connector.inputs.push(connectorControl);

  renderer.renderCollection = [connector,connector_2,yeah,ohno];
  renderer.inputCollection = [connector];

  renderer.scene(next);

}
