
function inter_2(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/inter.mp3');
  audio.loop = true;
  audio.play();

  var background = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/destroyerBack.png",40,512,512,256,256,true,1.0);
  // background.setLoopInterval(10);

  // var title = new text(renderer.midpoint.x,renderer.midpoint.y,16,16,0,"rgb(255,255,255)",1.0,1.0,"Destroyer","Courier","center");
  var title = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/destructorIntro.png",40,512,512,256,256,true,1.0);

  renderer.renderCollection = [background,title];
  renderer.inputCollection = [];

  function moveOn(){
    audio.pause();
    renderer.play = false;
  }

  setTimeout(moveOn,2500);
  renderer.scene(next);

}
