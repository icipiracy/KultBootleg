
function scene_2_score(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/inter.mp3');
  audio.loop = true;
  audio.play();

  console.log(sessionStorage.mountCount)

  var background = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/warheads_score.png",0,512,512,512,512,false,1.0);
  var title = new sprite(renderer.midpoint.x,50,0,"images/destructorIntro.png",40,250,250,256,256,true,1.0);
  var score = new text(renderer.midpoint.x + 20,512-140,32,32,0,"rgb(0,0,0)",1.0,1.0,sessionStorage.mountCount,"Courier","center");

  renderer.renderCollection = [background,title,score];
  renderer.inputCollection = [];

  function moveOn(){
    audio.pause();
    renderer.play = false;
  }

  setTimeout(moveOn,5000);
  renderer.scene(next);

}
