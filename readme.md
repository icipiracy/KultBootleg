# Suckadelic - Icipiracy Video Game
## "Bootleg toy exhibition"
### Kult Gallery Singapore

This is the source code to a video game to aims to portray the inner workings of a bootleg corporation.
The game is programmed in javascript and is based on html5 canvas.

An interesting aspect of this project is that I wrote my own renderer as a way to learn about rendering graphics.

All sprites were modelled and animated by me in Blender 3D, and some sprites are based on images downloaded from the internet.
A python script was used to stitch rendered frames from blender together into a single image so that it could be used as a sprite.
