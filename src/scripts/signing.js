
function signing(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/inter.mp3');
  audio.loop = true;
  audio.play();

  var contract = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/contract.png",0,512,512,512,512,false,1.0);
  var signatures = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/signatures.png",0,512,512,512,512,false,0.0);
  signatures.signed = false;

  var ohno = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/ohno.png",40,256,256,128,128,false,1.0);
  var yeah = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/yeah.png",40,256,256,128,128,false,1.0);

  var sign = new sprite(512-64,512-64,0,"images/sign.png",25,128+8,128+8,128,128,true,1.0);

  function signContract(el){
    if(keys['32'] || keys['mouse']){
      signatures.opacity = 1.0;
      signatures.signed = true;
    }
  }

  signatures.inputs.push(signContract);

  function moveOn(){

    if(signatures.signed === false){
      ohno.sprite_run = 1;
    }else if(signatures.signed === true){
      yeah.sprite_run = 1;
    }

    function am(){
      audio.pause();
      renderer.play = false;
    }
    setTimeout(am,1000);
  }

  renderer.renderCollection = [contract,sign,signatures,ohno,yeah];
  renderer.inputCollection = [signatures];

  setTimeout(moveOn,4000);
  renderer.scene(next);

}
