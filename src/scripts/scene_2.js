
function scene_2(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var siren = new Audio('sounds/siren.mp3');
  var audio = new Audio('sounds/scene_2.mp3');
  audio.loop = true;
  audio.play();

  var background = new plane(renderer.midpoint.x,renderer.midpoint.y,0,renderer.canvas.width,renderer.canvas.height,"rgb(0,0,0)");

  var destroyer = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/rocketTop.png",140,512,512,256,256,false,1.0);
  destroyer.sprite_run = 1;
  destroyer.sprite_run_once = true;
  destroyer.sprite_run_frames = 20;

  var mountCount = 0;

  destroyer.onRender = function(el){
    if(el.sprite_current_frame == 20){
      nuclear.opacity = 1.0;
      nuclear.active = true;
    }
    if(el.sprite_current_frame >= 139){
      el.sprite_run = 1;
      mountCount += 1;
      sessionStorage.mountCount = mountCount;
      el.sprite_run_once = true;
      el.sprite_run_frames = 20;
      el.sprite_current_frame = 0;
    }
  }

  var nuclear = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/mountReady.png",40,256,256,128,128,true,0.0);
  nuclear.active = false;

  var yeah = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/yeah.png",40,256,256,128,128,false,1.0);

  var warheads_mounted = new sprite(renderer.midpoint.x,512-100,0,"images/warheads_mounted.png",0,256,256,256,256,false,1.0);
  var mount_number = new text(renderer.midpoint.x,512-40,64,64,0,"rgb(255,255,255)",1.0,1.0,"0","Courier","center");

  function moveOn(){
    audio.pause();
    renderer.play = false;
  }

  function control(el){
    if(keys['32'] || keys['mouse']){
      if(el.active){

        yeah.sprite_run = 1;

        destroyer.sprite_run_frames = 140 - 20;
        el.opacity = 0.0;
        el.active = false;
        mount_number.textValue = mountCount;
      }
    }
  }

  setTimeout(moveOn,20000);

  nuclear.inputs.push(control);

  renderer.renderCollection = [background,destroyer,nuclear,warheads_mounted,mount_number,yeah];
  renderer.inputCollection = [nuclear];

  renderer.scene(next);

}
