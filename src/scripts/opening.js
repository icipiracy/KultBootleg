
function opening(next){

  var renderer = new mainRender();
  renderer.frame_rate = 40;

  var audio = new Audio('sounds/intro.mp3');
  audio.loop = true;
  audio.play();

  // var background = new plane(renderer.midpoint.x,renderer.midpoint.y,0.0,renderer.ctx.canvas.width,renderer.ctx.canvas.height,"rgb(255,255,255)",1.0,1.0);

  var icipiracy = new sprite(renderer.midpoint.x, renderer.midpoint.y - 200,0,"images/icipiracy_letters.png",40,256,256,512,512,true,1.0);
  icipiracy.run = -1;
  icipiracy.setLoopInterval(20);

  var suckadelic = new sprite(renderer.midpoint.x, renderer.midpoint.y - 160,0,"images/suckadelic_letters.png",40,256,256,512,512,true,1.0);
  suckadelic.run = -1;
  suckadelic.setLoopInterval(20);

  var factory = new sprite(renderer.midpoint.x, renderer.midpoint.y,0,"images/factory.png",20,256+64,256+64,256,256,true,1.0);
  factory.run = -1;
  factory.setLoopInterval(10);

  var start = new sprite(renderer.midpoint.x,renderer.midpoint.y + 200,0,"images/start.png",0,200,40,200,40,false,1.0);
  start.started = false;
  start.lol = "hello";
  start.animation_relative = true;

  var background = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/backgroundflash.png",40,512,512,256,256,true,1.0);

  start.animation_frames = [{
  'x': 1.0,
  'y': 1.0,
  'scale': 1.0,
  'color': "",
  'angle': 0,
  'opacity': 1.0,
  'width': 1.0,
  'height': 1.0
  },
  {
  'x': 1.0,
  'y': 1.0,
  'scale': 1.0,
  'color': "",
  'angle': 0,
  'opacity': 1.0,
  'width': 1.05,
  'height': 1.05
  },
  {
  'x': 1.0,
  'y': 1.0,
  'scale': 1.0,
  'color': "",
  'angle': 0,
  'opacity': 1.0,
  'width': 1.015,
  'height': 1.015
  },
  {
  'x': 1.0,
  'y': 1.0,
  'scale': 1.0,
  'color': "",
  'angle': 0,
  'opacity': 1.0,
  'width': 1.02,
  'height': 1.02
  },
  {
  'x': 1.0,
  'y': 1.0,
  'scale': 1.0,
  'color': "",
  'angle': 0,
  'opacity': 1.0,
  'width': 1.015,
  'height': 1.015
  },
  {
  'x': 1.0,
  'y': 1.0,
  'scale': 1.0,
  'color': "",
  'angle': 0,
  'opacity': 1.0,
  'width': 1.01,
  'height': 1.01
  },
  {
  'x': 1.0,
  'y': 1.0,
  'scale': 1.0,
  'color': "",
  'angle': 0,
  'opacity': 1.0,
  'width': 1.005,
  'height': 1.005
  },
  ];

  start.animation_play = true;
  // start.animation_run = 8;
  start.animation_loop = true;

  var sw = false;

  function enableSwitch(){
    sw = true;
  };

  setTimeout(enableSwitch,1000);

  function startGame(el){
    if(sw){
      if(keys['32'] || keys["mouse"]){
        if(el.started != true){
          el.started = true;
          function moveOn(){
            audio.pause();
            audio.currentTime = 0;
            renderer.play = false;
          }
          setTimeout(moveOn,1000);
        }
      }
    }
  };

  start.inputs.push(startGame);

  renderer.renderCollection = [background,icipiracy,suckadelic,start,factory];
  renderer.inputCollection = [start];

  renderer.scene(next);

}
