
function ending(next,frame_rate){

  console.log("ENDING");

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/inter.mp3');
  audio.loop = true;
  audio.play();

  var icipiracy = new sprite(renderer.midpoint.x, renderer.midpoint.y - 200,0,"images/icipiracy_letters.png",40,256,256,512,512,true,1.0);
  icipiracy.run = -1;
  icipiracy.setLoopInterval(20);

  var suckadelic = new sprite(renderer.midpoint.x, renderer.midpoint.y - 160,0,"images/suckadelic_letters.png",40,256,256,512,512,true,1.0);
  suckadelic.run = -1;
  suckadelic.setLoopInterval(20);

  var background = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/end_titles.png",40,512,512,256,256,true,1.0);

  // var restart_title = new text(renderer.midpoint.x,512-128,16,16,0,"rgb(255,255,255)",1.0,1.0,"Start Over","Courier","center");

  function restart(el){
    if(keys['mouse'] || keys['32']){
      renderer.play = false;
      audio.pause();
      startOver();
    }
  }

  background.inputs.push(restart);

  renderer.renderCollection = [background,icipiracy,suckadelic];
  renderer.inputCollection = [background];

  renderer.scene(next);

}
