// Copright (c) Igor Hoogerwoord 2017
// Read LICENSE.md for a MIT license thingy about this code.

var DB = {};
DB.parameters = {};
DB.parameters.location = "";
DB.parameters.user = {};
DB.URLs = {};
DB.threads = [];
DB.currentthreadid = 0;
DB.unique_number = 0;
DB.parameters.location = window.location.origin;
DB.system = {};

var here = window.location.href;	//URL

console.log("lzs v1.0.1");

var SH = {};
SH.randomString = function(length){

  var N = length;

  var random = Array(N+1).join((Math.random().toString(36)+'00000000000000000').slice(2, 18)).slice(0, N);

  return random;
};
SH.jsonCheck = function(string){
  try{
    JSON.parse(string);
  } catch(e){
    return false;
  }
  return true;
}
SH.contains = function(substring,string){
  if(string.indexOf(substring) == -1){
    return false;
  } else {
    return true;
  };
};
SH.add_select_option = function(list_id,input_id){

  var title = document.getElementById(input_id).value;
  var projects_list = document.getElementById(list_id);

  if(title != ""){

    var status = "correct";

    for(var i = 0; i < projects_list.length; i++) {
      if(title != projects_list.options[i].text){
        status = "correct";
      }else{
        status = "name_error"
      };
    };
    if(status == "name_error"){
    }else if(status == "correct"){
      var option = document.createElement("option");
      option.text = title;
      projects_list.add(option);
    };
  }else{
    console.log('please set a project name.')
  };
};
SH.isInArray = function(value, list){

  for(i=0; i<list.length; i++) {
      if(list[i]==value) {
          return true;
      };
  };
  return false;

};
SH.fill_select = function(list,select){

  if(typeof select === 'string'){
    select = document.getElementById(select);
  };

  SH.empty_select(select);

  for(var gss = 0; gss < list.length; gss ++){

    var option = document.createElement('option');

    option.text = list[gss];
    select.add(option);

  };

};
SH.empty_select = function(selectbox){

    var i;

    for(i=selectbox.options.length-1;i>=0;i--){
      selectbox.remove(i);
    };

};
SH.get_select_value = function(elt){

  //var elt = document.getElementById(id);

  if (elt.selectedIndex == -1)
      return null;

  return elt.options[elt.selectedIndex].text;
};
SH.set_select_value = function(sel,val){

  var opts = sel.options;
  for(var opt, j = 0; opt = opts[j]; j++) {
    if(opt.value == val) {
        sel.selectedIndex = j;
        break;
    };
  };

};
SH.array_remove = function(list,value){

    var i = SH.array_index(list,value);

    if(i != -1) {
  	   list.splice(i, 1);
    };

    return list;
};
SH.array_index = function(list,value){

  for(i=0; i<list.length; i++) {
      if(list[i]==value) {
          return i;
      };
  };

};
SH.request = function(body,callback,url,headers,method){

  // Body must be string encoded.
  // Callback url will be passed a xhrresponse object.
  // Headers must be object where key = header name and value is header content.

  if(method != "POST" && method != "GET" && method != "PUT"){
    return false
  }else{

    if(url == undefined || url == ''){
      url = '/';
    };

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function(){

  		var xhrresponse = "";

  		if (xhr.readyState === xhr.DONE){
  			if (xhr.status === 200 || xhr.status === 0){
  				if (xhr.responseText){

  					xhrresponse = xhr.responseText;

  				}else{
  					xhrresponse = "HttpRequest[" + url + "] no response text";
  				};
  			}else{
  				xhrresponse = "HttpRequest[" + url + "] status: " + xhr.status;
  			};
        if(callback != undefined){
          callback(xhrresponse);
        };
  		};
  	};

  	xhr.open(method,url,true);

    for(var x in headers){
      xhr.setRequestHeader(x,headers[x]);
    };

  	xhr.setRequestHeader("Content-Type","text/plain");
  	xhr.send(JSON.stringify(body));
    return true;
  };
};

SH.response = function(response){

  // Standard json decoding of response used for quick debugging

  var result = "";

  if(SH.jsonCheck(response) == true){
    result = JSON.parse(response);
  }else{
    result = response;
  };

  return result;

};

SH.navigate = function(parameters){

  var href = "/" + parameters.url;
  window.location.href = href;

};
SH.checkemail = function(email){

  var testresults;
  var str = email;
  var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

  if(filter.test(str)){
      testresults=true;
  }else{
      testresults=false;
  };
  return testresults;

};
SH.upload = function(parameters,callbacks){

  /*

    Takes 3 properties, of which url and ftype are important variables.
    ftype is the content-type determined by the server for this file.
    url, is the server signed-url to which this file can be uploaded.
    file is the javascript object which is eventually uploaded.

    Some xmlhttp callbacks implemented.

  */

  var url = parameters.url;
  var file = parameters.file;
  var ftype = parameters.ftype;

  var xhr = new XMLHttpRequest();

  xhr.progressId = url;
  xhr.upload.progressId = url;
  xhr.callbacks = callbacks;

  xhr.onreadystatechange = function(){

    var xhrresponse = "";
    var haserror = true;

    if (xhr.readyState === xhr.DONE){
      if (xhr.status === 200 || xhr.status === 0){
        if (xhr.responseText){

          xhrresponse = xhr.responseText;
        }else{
          xhrresponse = "HttpRequest[" + url + "] no response text";
        };
      }else{
        xhrresponse = "HttpRequest[" + url + "] status: " + xhr.status;
      };
      // Assign global parameters to retrieve file info in callback:
      if(callbacks != undefined){
        xhr.parameters = parameters;
        callbacks.uploadCallback(xhr);
      };
    };
  };

  // Assign global parameters to retrieve file info in callback:
  if(callbacks != undefined){
    xhr.upload.onprogress = function(e){
      e.parameters = parameters;
      callbacks.progressCallback(e);
    };

    xhr.upload.onabort = function(e){
      e.parameters = parameters;
      callbacks.errorCallback(e);
    };
    xhr.upload.onerror = function(e){
      e.parameters = parameters;
      callbacks.errorCallback(e);
    };
    xhr.upload.ontimeout = function(e){
      e.parameters = parameters;
      callbacks.errorCallback(e);
    };
  }else{
    return false;
  };

  xhr.open("PUT",url);
  xhr.setRequestHeader("content-type", ftype);
  xhr.send(file);

};
SH.load_progress = function(response){

  if(response.lengthComputable){
    var completion = response.loaded / response.total;
    var rounded = Math.round(completion * 10) / 10;
    var percent = rounded * 100;
    return percent;
  }else{
    console.log("unable to determine completion")
  };

};
SH.download = function(get_url,callback,downloadProgress){

  if(DB.parameters.sessionid === "none"){
    return;
  };

  var xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function(){
    var xhrresponse = "";
    var haserror = true;

    if (xhr.readyState === xhr.DONE){
      if (xhr.status === 200 || xhr.status === 0){
        if (xhr.responseText){

          xhrresponse = xhr.responseText;
        }else{
          xhrresponse = "HttpRequest[" + get_url + "] no response text";
        };
      }else{
        xhrresponse = "HttpRequest[" + get_url + "] status: " + xhr.status;
      };
      if(callback != undefined){
        callback(xhrresponse);
      };
    };
  };

  if(downloadProgress != undefined){
    xhr.upload.onprogress = downloadProgress;
  };

  xhr.open("GET",get_url,true);
  xhr.send( null );

};
SH.line_breaks = function(text){

  text = text.replace(/\r\n/g, '<br/>').replace(/[\r\n]/g, '<br/>');
  return text;

};
SH.r_line_breaks = function(text){

  text = text.replace(/<br\s*[\/]?>/gi, "\n");
  return text;

};
SH.urlify = function(text) {

  var urlRegex = (/(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel|local|internal))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)(\s+|$)/gi)

  return text.replace(urlRegex, function(url) {
      return '<a href="' + url + '">' + url + '</a>';
  })
};
SH.isEmpty = function(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    };

    return true;
};
SH.clone = function(obj) {
    var copy;

    if (null == obj || "object" != typeof obj){
      return obj;
    };

    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }
    if (obj instanceof Object) {
        copy = {};
        for (var attribute in obj) {
            if (obj.hasOwnProperty(attribute)) copy[attribute] = SH.clone(obj[attribute]);
        }
        return copy;
    };

    throw new Error("Copy object error - unsupported type");
};
SH.makeUnselectable = function(node) {
    if (node.nodeType == 1) {
        node.setAttribute("unselectable", "on");
        node.style.userSelect = 'none';
    };
    var child = node.firstChild;
    while (child) {
        SH.makeUnselectable(child);
        child = child.nextSibling;
    };
};
SH.numberToHEX = function(nn){
  hexString = nn.toString(16);
  return hexString;
};
SH.escapeXML = function(str){
    if(typeof str.replace == "function"){
      return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
    }else{
      return str;
    };
};
SH.exportToCsv = function(filename, csvFile) {

  var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });

  if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, filename);
  }else{
    var link = document.createElement("a");
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      var url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", filename);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    };
  };

};
SH.save_data = function(data){
  localStorage.save = JSON.stringify(data);
};
SH.get_data = function(){
  return JSON.parse(localStorage.save);
};
SH.secure_redirect = function(link,param){

  link += "?" + param[0][0] + "=" + param[0][1];

  if(param.length > 0){
    for(var ix = 1;ix <  param.length; ix++){
      link += "&" + param[ix][0] + "=" + param[ix][1];
    };
  };

  location.href = link;
};
SH.uuid = function(){
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

var UI = {};
UI.init = function(){

  console.log("lzs ui init.");

  UI.button_ix_load();
  UI.apply_autosize();
}
UI.buttonIX = function(element){
  element.onmouseenter = function(){
    element.style.opacity = "0.98";
    element.style.cursor = "pointer";
    element.style.WebkitTransition = '-webkit-transform 0.1s';
    element.style.transition = 'transform 0.1s';
    element.style.WebkitTransform = "translateY(-2px)";
    element.style.msTransform = "translateY(-2px)";
    element.style.transform = "translateY(-2px)";
    element.style.boxShadow = "0px 2px 10px rgb(220,220,220)";
  };
  element.onmouseleave = function(){
    element.style.position = "";
    element.style.cursor = '';
    element.style.opacity = "1";
    element.style.WebkitTransform = "translateY(0px)";
    element.style.msTransform = "translateY(0px)";
    element.style.transform = "translateY(0px)";
    element.style.boxShadow = "0px 0px 7px rgb(240,240,240)";
  };
  element.onmouseover = function(){
    element.style.opacity = "0.98";
    element.style.cursor = "pointer";
    element.style.WebkitTransition = '-webkit-transform 0.1s';
    element.style.transition = 'transform 0.1s';
    element.style.WebkitTransform = "translateY(-2px)";
    element.style.msTransform = "translateY(-2px)";
    element.style.transform = "translateY(-2px)";
    element.style.boxShadow = "0px 2px 10px rgb(220,220,220)";
  };
  element.onmouseout = function(){
    element.style.position = "";
    element.style.cursor = '';
    element.style.opacity = "1";
    element.style.WebkitTransform = "translateY(0px)";
    element.style.msTransform = "translateY(0px)";
    element.style.transform = "translateY(0px)";
    element.style.boxShadow = "0px 0px 7px rgb(240,240,240)";
  };
  element.onmousedown = function(){
    element.style.opacity = "0.9";
    element.style.cursor = "pointer";
    element.style.WebkitTransform = "translateY(0px)";
    element.style.msTransform = "translateY(0px)";
    element.style.transform = "translateY(0px)";
    element.style.boxShadow = "0px 0px 7px rgb(240,240,240)";
  };
  element.onmouseup = function(){
    element.style.position = "";
    element.style.cursor = '';
    element.style.opacity = "1";
    element.style.WebkitTransform = "translateY(-2px)";
    element.style.msTransform = "translateY(-2px)";
    element.style.transform = "translateY(-2px)";
    element.style.boxShadow = "0px 2px 10px rgb(220,220,220)";
  };

  return element
};
UI.remove_IX = function(element){
  element.onmouseenter = '';
  element.onmouseleave = '';
  element.onfocus = '';
  element.onblur = '';
  element.style.cursor = 'auto';
};
UI.elementDisplay = function(id,display){
  el = document.getElementById(id);
  el.style.display = display;
};
UI.addProgressBar = function(parent,width,classes){

  var div = document.createElement('DIV');
  div.className = classes['div'];
  div.style.width = width;

  var par = document.createElement('P');
  par.className = classes['p'];
  par.id = SH.randomString(10);
  div.appendChild(par);

  var bardiv = document.createElement('DIV');
  bardiv.className = classes['bar'];
  div.appendChild(bardiv);

  var bar = document.createElement('DIV');
  bar.className = classes['load'];
  bar.style.width = "0%";
  bar.id = SH.randomString(10);

  bardiv.appendChild(bar);
  parent.appendChild(div);

  var ids = {};
  ids.par = par.id;
  ids.bar = bar.id;

  return ids;
};

UI.colors_flat = {};
UI.colors_flat.red = '#fc4551';
UI.colors_flat.blue = '#42aef1';
UI.colors_flat.green = '#4cf196';
UI.colors_flat.yellow = '#fcc726';
UI.colors_flat.grey = 'rgb(150,150,150)';
UI.list_button = function(parent,data,ui_function){

  var display_button = document.createElement("A");
  display_button.className = "w-inline-block news_item_button list_item_button";
  display_button.dataset = data;
  display_button.onclick = ui_function;
  display_button.href = "#";

  parent.appendChild(display_button);

  UI.buttonIX(display_button);

  var display_icon = document.createElement("IMG");
  display_icon.className = "down_arrow";
  display_icon.src = "resources/images/down_arrow.png";
  display_icon.width = "30";

  display_button.appendChild(display_icon);

};
UI.cross_button = function(parent,data,ui_function){

  var cross_button = document.createElement('A');
  cross_button.className = "w-inline-block news_item_button list_item_button";
  cross_button.href = "#";
  cross_button.dataset = data;
  cross_button.onclick = ui_function;

  UI.buttonIX(cross_button);

  var cross_image = document.createElement('IMG');
  cross_image.className = 'down_arrow';
  cross_image.src = 'resources/images/cross.png';
  cross_image.width = '35px';

  cross_button.appendChild(cross_image);
  parent_appendChild(cross_button);

}
UI.button_ix_load = function(){

  var btns = document.getElementsByTagName("A");

  for(var bsz in btns){
    var el = btns[bsz];
    UI.buttonIX(el);
  };

  var aaz = document.getElementsByTagName('BUTTON');

  for(var bsz in aaz){
    var el = aaz[bsz];
    UI.buttonIX(el);
  };

  var aaf = document.getElementsByTagName('LABEL');

  for(var bsz in aaf){
    var el = aaf[bsz];
    UI.buttonIX(el);
  };

};
UI.setProgressbar = function(id, percent){
  var bar = document.getElementById(id);
  percent = percent.toString() + "%";
  bar.style.width = percent;
};
UI.input_IX = function(element){

  element.style.borderColor = 'rgba(0,0,0,0)';

  element.onfocus = function(){
    element.style.borderColor = 'rgba(0,0,0,1)';
  };

  element.onblur = function(){
    element.style.borderColor = 'rgba(0,0,0,0)';
  };

};
UI.apply_autosize = function(){

  autosize(document.querySelectorAll('textarea'));

};
UI.autosize_update = function(){

  var ta = document.querySelector('textarea');

  autosize(ta);

  var evt = document.createEvent('Event');

  evt.initEvent('autosize:update', true, false);

  ta.dispatchEvent(evt);

};
UI.area_resize = function(area){
  area.style.height = 'auto';
  area.style.height = area.scrollHeight+'px';
};
UI.basicForm = function(intiationButton,sessionStorageField,propertyList,editDiv,returnFunction,initiationButtonText,formValues){

  /*

  propertyList and formValues must be identical except for the differing values.
  formValues is optional and must contain input field values.

  */

  var sediv = editDiv;
  var btn = intiationButton;
  var se_fields = propertyList;

  if(sessionStorage[sessionStorageField] == 'true'){

    sediv.style.display = "none";
    btn.innerHTML = "";
    btn.appendChild(document.createTextNode(initiationButtonText));

    sessionStorage[sessionStorageField] = 'false';

  }else if(sessionStorage[sessionStorageField] == 'false' || sessionStorage[sessionStorageField] == undefined){

    var inputs = {}

    if(sediv.addedFields != true){

      for(var idx in se_fields){

        var inp = html._input('text');
        inp.id = se_fields[idx];
        inp.placeholder = se_fields[idx];

        if(formValues != undefined){
          inp.value = formValues[se_fields[idx]];
        }

        var plabel = html.p();
        plabel.appendChild(document.createTextNode(se_fields[idx]));

        inputs[se_fields[idx] + "_label"] = plabel;
        inputs[se_fields[idx]] = inp;

      };

      for (var key in inputs) {
        if (inputs.hasOwnProperty(key)) {
          sediv.appendChild(inputs[key]);
          // sediv.appendChild(html.br());
        };
      };

      var saveButton = html.button();
      saveButton.onclick = returnFunction;
      saveButton.inputsDiv = sediv;
      saveButton.appendChild(document.createTextNode('Save'));

      sediv.appendChild(saveButton);
      sediv.appendChild(html.br());

    };

    sediv.addedFields = true;

    sessionStorage[sessionStorageField] = 'true';
    btn.innerHTML = "";
    btn.appendChild(document.createTextNode("Back"));
    sediv.style.display = "";

  };

};
UI.selected_index = function(svb){

  /*

  Set selected element color to green.
  Set previous selected element color back to class default.
  Requirements:
    - 'index_type' property added to html element, to indicate to what selector the element belongs

  */

  var prefix = "";

  if(svb.index_type != "" || svb.index_type != undefined){
    prefix = svb.index_type + "_";
  };

  if(sessionStorage[prefix + "selected"] != "" || sessionStorage[prefix + "selected"] != undefined){
    sessionStorage[prefix + "previous"] = sessionStorage[prefix + "selected"];
  };

  sessionStorage[prefix + "selected"] = svb.id;

  var selected = html.byId(sessionStorage[prefix + "selected"]);
  var previous = html.byId(sessionStorage[prefix + "previous"]);

  selected.style.backgroundColor = UI.colors_flat.green;

  if(previous != null || previous != undefined){
    previous.style.backgroundColor = "";
  };

  // Taking into account that the same button can be pressed twice:
  if(previous == undefined){
    previous = {};
    previous.id = "";
  };

  if(selected.id === previous.id){
    if(selected.aState == undefined){
      selected.aState = false;
    };

    if(selected.aState === true){
      selected.style.backgroundColor = UI.colors_flat.green;
    }else if(selected.aState === false){
      selected.style.backgroundColor = "";
    };

  };

};


// This is maximum lazy:
var html = {};
html.div = function(cssname){
  var div = document.createElement('DIV');
  div.id = SH.randomString(10);
  if(cssname != undefined && cssname != ""){
    div.className = cssname;
  };
  return div;
};
html.p = function(cssname){

  var par = document.createElement('P');
  par.id = SH.randomString(10);

  if(cssname != undefined && cssname != ''){
    par.className = cssname;
  };

  return par;
};
html.h = function(cssname){

  var hdr = document.createElement('H');
  hdr.id = SH.randomString(10);

  if(cssname != undefined && cssname != ''){
    hdr.className = cssname;
  };

  return hdr
};
html._input = function(type,cssname){
  var inp = document.createElement('INPUT');
  if(type == ''){
    inp.type = 'text';
  }else{
    inp.type = type;
  };
  inp.id = SH.randomString(10);
  if(cssname != undefined && cssname != ""){
    inp.className = cssname;
  };
  return inp;
};
html.button = function(cssname){

  var btn = document.createElement('A');

  if(cssname != undefined && cssname != ''){
    btn.className = cssname;
  };

  UI.buttonIX(btn);
  btn.id = SH.randomString(10);

  return btn;
};
html.textarea = function(auto,cssname){
  var txtr = document.createElement('textarea');
  if(auto == true){
    autosize(txtr);
  };
  if(cssname != undefined && cssname != ''){
    txtr.className = cssname;
  };
  txtr.id = SH.randomString(10);
  return txtr;
};
html._select = function(cssname,options){

  var sl = document.createElement('select');
  sl.id = SH.randomString(10);
  SH.fill_select(options,sl);

  if(cssname != undefined && cssname != ''){
    sl.className = cssname;
  };

  return sl;

};
html.img = function(src,cssname){
  var img_n = document.createElement('img');
  img_n.src = src;
  if(cssname != undefined && cssname != ''){
    img_n.className = cssname;
  };
  return img_n;
};
html.br = function(){
  return document.createElement('br');
};
html.byId = function(id){
  return document.getElementById(id);
};
html.byCss = function(css){
  return document.getElementsByClassName(css);
};
html.byTag = function(tag){
  return document.getElementsByTagName(tag);
};
html.getVal = function(id){
  return document.getElementById(id).value;
};
html.set_this = function(id){
  var el = html.byId(id);
  el.value = el.value;
};
html.setVal = function(id,val){
  document.getElementById(id).value = val;
  return true;
};
html.appendMultiple = function(parentElement,childElements){
  for(var ix in childElements){
    parentElement.appendChild(childElements[ix]);
  };
};

UI.init();
