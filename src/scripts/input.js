function keyInput(event) {
    var x = event.which || event.keyCode;
}


var keys = {};
keys.tracked = []

// function addListener(keyCode,eventHandler){
//   keys[keyCode] = eventHandler;
// }

function keyDown(event){

  if(event.type != undefined && event.type == "mousedown"){
    keys["mouse"] = true;
  }else{
    keys[event.which.toString()] = true;
  }

}

function keyUp(event){

  if(event.type != undefined && event.type == "mouseup"){
    keys["mouse"] = false;
  }else{
    keys[event.which.toString()] = false;
  }

}

function loadKeyReference(){

  function loadC(response){

      var data = Papa.parse(response);

      var keyRef = {};

      for(var i = 0; i < data.data.length; i++){
        keyRef[data.data[i][1]] = data.data[i][0]
      }

  }

  SH.download("keyReference.csv",loadC,SH.load_progress);

};
