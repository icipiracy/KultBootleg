
function scene_4_score(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/scene_2.mp3');
  audio.loop = true;
  audio.play();

  var doll_complete = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/doll_complete.png",60,512,512,256,256,false,1.0);
  doll_complete.sprite_run = 1;
  doll_complete.sprite_run_once_and_pause = true;
  var bootlegMaster = new sprite(128,256,0,"images/bootlegMaster.png",20,128,128,256,256,true,1.0);

  function moveOn(){
    audio.pause();
    renderer.play = false;
  }

  setTimeout(moveOn,4500);

  renderer.renderCollection = [doll_complete,bootlegMaster];
  renderer.inputCollection = [];

  renderer.scene(next);

}
