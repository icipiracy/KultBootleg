
function tutorial(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/inter.mp3');
  audio.loop = true;
  audio.play();

  var background = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/guide.png",0,512,512,512,512,false,1.0);
  var mash = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/mash.png",20,128,128,128,128,true,1.0);

  renderer.renderCollection = [background,mash];
  renderer.inputCollection = [];

  function moveOn(){
    audio.pause();
    renderer.play = false;
  }

  setTimeout(moveOn,6000);
  renderer.scene(next);

}
