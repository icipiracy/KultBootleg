
function inter_3(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/inter.mp3');
  audio.loop = true;
  audio.play();

  var background = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/assembler_back.png",0,512,512,512,512,false,1.0);

  // var title = new text(renderer.midpoint.x,renderer.midpoint.y,16,16,0,"rgb(255,255,255)",1.0,1.0,"Destroyer","Courier","center");
  var title = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/assembler_title.png",40,256+64,256+64,256,256,true,1.0);
  title.sprite_loop_interval = 10;

  var sign = new sprite(512-64,512-64,0,"images/sign.png",25,128+16,128+16,128,128,true,1.0);

  var signatures = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/assembler_signature.png",0,512,512,512,512,false,0.0);

  function signContract(el){
    if(keys['32'] || keys['mouse']){
      signatures.opacity = 1.0;
    }
  }

  signatures.inputs.push(signContract);

  renderer.renderCollection = [background,title,signatures,sign];
  renderer.inputCollection = [signatures];

  function moveOn(){
    audio.pause();
    renderer.play = false;
  }

  setTimeout(moveOn,6000);
  renderer.scene(next);

}
