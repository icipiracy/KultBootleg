
function scene_1(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/scene_1.mp3');
  audio.loop = true;
  audio.play();

  var timer = 100;
  var timeLeft = timer * 100;
  console.log(timeLeft);

  var background = new plane(renderer.midpoint.x,renderer.midpoint.y,0,renderer.canvas.width,renderer.canvas.height,"rgb(0,0,0)");

  var mash = new sprite(512-64,512-64,0,"images/spin.png",25,128+16,128+16,128,128,true,1.0);
  var complete = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/complete.png",20,256,256,128,128,true,0.0);
  complete.success = false;

  // var counter = new sprite(80,512-80,0,"images/constCount.png",40,256,256,128,128,true,1.0);

  var timerText = new text(renderer.midpoint.x,512-32,16,16,0,"rgb(255,0,0)",1.0,1.0,timeLeft.toString(),"Courier","center");

  function updateTimer(){
    timeLeft -= 4;
    timerText.textValue = "TIME LEFT: " + timeLeft.toString();
  }

  var ohno = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/ohno.png",40,256,256,128,128,false,1.0);

  var iid = setInterval(updateTimer,1);

  var body = new sprite(renderer.midpoint.x, renderer.midpoint.y,0,"images/body.png",51,512,512,256,256,false,1.0);
  body.sprite_run_once = true;
  body.count = 5;
  body.steps = 5;
  body.sprite_run_once_and_pause = true;
  body.active = true;

  var wheel = new sprite(512-128,128,0,"images/wheel.png",41,256,256,128,128,false,1.0);

  var bar = new sprite(512-70,120,0,"images/healthBar.png",10,128,128,64,64,false,1.0);
  bar.sprite_run_once_and_pause = true;
  bar.sprite_run_once = true;
  bar.opacity = 0.0;

  function sceneEnd(){
    function end(){
      audio.pause();
      audio.currentTime = 0;
      renderer.play = false;
    }
    setTimeout(end,2000);
  }

  function spriteLaunch(el){
    if(keys['32'] || keys["mouse"]){
      if(el.pressed == false){
        el.sprite_run = 1;
        el.sprite_run_once = true;
        if(el.sprite_current_frame < el.sprite_frames - 1){
          el.sprite_run_frames = 10;
        }else{
          el.sprite_current_frame = 0;
        }
        el.pressed = true;
      }
    }else{
      el.pressed = false;
    }
  }

  function bodyControl(el){

    if(body.active){
      if(keys['32'] || keys["mouse"]){
        if(el.pressed == false){

          var setCount = 5;

          if(el.count > 0){
            el.count -= 1;
            bar.sprite_current_frame += 2;
          }else{
            el.count = setCount;
            bar.sprite_current_frame = 0;
            el.steps -= 1;
            console.log("count: " + el.count + " steps: " + el.steps);
          }

          if(el.count == 0){
            if(el.sprite_current_frame < el.sprite_frames){
              el.sprite_run = 1;
              el.sprite_run_frames = setCount;
            }
          }

          if(el.sprite_current_frame == 50){
            complete.opacity = 1.0;
            complete.success = true;
            sceneEnd();
          }

          el.pressed = true;

        }
      }else{
        el.pressed = false;
      }
    }

  }


  function moveOn(){

    if(complete.success == false){
      ohno.sprite_run = 1;
    }

    function actuallyMoveOn(){
      audio.pause();
      renderer.play = false;
    }

    setTimeout(actuallyMoveOn,1000);

  }

  setTimeout(moveOn,timer * 100);

  body.inputs.push(bodyControl);
  wheel.inputs.push(spriteLaunch);

  renderer.renderCollection = [background,body,wheel,bar,timerText,mash,complete,ohno];
  renderer.inputCollection = [body,wheel];

  renderer.scene(next);

}
