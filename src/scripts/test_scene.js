
function test_scene(){

  var renderer = new mainRender();

  var background = new plane(renderer.midpoint.x,renderer.midpoint.y,0,renderer.canvas.width,renderer.canvas.height,"rgb(0,0,0)");
  var ball = new circle(renderer.midpoint.x,renderer.midpoint.y,0,20,"rgb(200,200,200)");
  var ball2 = new circle(renderer.midpoint.x,renderer.midpoint.y,0,20,"rgb(200,150,150)");

  // var test_sprite = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/cube_test.png",20,200,200,100,100,false);
  var body = new sprite(renderer.midpoint.x, renderer.midpoint.y,0,"images/body.png",50,200,200,200,200,false);
  var buste = new sprite(renderer.midpoint.x, renderer.midpoint.y - 95,0,"images/buste_head.png",50,200,200,256,256,false);
  var casing = new sprite(renderer.midpoint.x, renderer.midpoint.y - 20,0,"images/casing.png",30,340,340,200,200,false);
  casing.count = 10;

  ball.addCollision([background,ball2]);

  function ballControl(el){

    var control_speed = 2;

    if(keys['87']){
      el.y -= control_speed;
    }
    if(keys['83']){
      el.y += control_speed;
    }
    if(keys['65']){
      el.x -= control_speed;
    }
    if(keys['68']){
      el.x += control_speed;
    }

  }

  function spritePlay(el){
    if(keys['32']){
      el.run = 1;
      el.current_frame = 0;
    }
  }

  function spriteStep(el){
    if(keys['32']){
      if(el.pressed == false){
        if(el.current_frame < el.frames - 1){
          el.current_frame += 1;
          el.pressed = true;
        }
      }
    }else{
      el.pressed = false;
    }
  }

  function spriteLaunch(el){
    if(keys['32']){
      if(el.pressed == false){
        el.run = 1;
        el.run_once = true;
        if(el.current_frame < el.frames - 1){
          el.run_frames = 10;
        }
        el.pressed = true;
      }
    }else{
      el.pressed = false;
    }
  }

  function casingControl(el){
    if(keys['32']){
      el.count -= 1;
      console.log(el.count);
      if(el.count < 1){
          el.run = 1;
          el.current_frame = 0
      }
    }
  }

  // ball2.inputs.push(ballControl);
  // test_sprite.inputs.push(spritePlay);
  buste.inputs.push(spriteLaunch);
  body.inputs.push(spriteLaunch);
  casing.inputs.push(casingControl);

  renderer.renderCollection = [background,buste,body,casing];
  renderer.inputCollection = [body,buste,casing];

  function draw() {

    renderer.clear();

    // if(ball.detect(background,true,20)[1]){
    //   ball.x = canvas.width / 2;
    //   ball.y = canvas.height / 2;
    // }else{
    //   ball.x += 2;
    //   ball.y += Math.PI;
    // }
    //
    // if(ball.detect(ball2,false,10)[1]){
    //   ball.color = "rgb(100,100,250)";
    // }else{
    //   ball.color = "rgb(200,200,200)";
    // }

    renderer.processInput();
    renderer.render();

  }

  setInterval(draw, 40);
}
