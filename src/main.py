import socket
import SimpleHTTPServer
import SocketServer
import json
from os import curdir, sep
import os
from urlparse import urlparse, parse_qs

class MyHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):

        url = self.path

        headers = parse_qs(urlparse(url).query)
        print "Get - incoming request:"
        print url
        print headers

        hdd = {}
        for idx in headers:
            hdd[idx] = headers[idx][0]

        if 'test' in hdd:
            if hdd['test'] == True:
                print 'test complete - url correctly parsed'
        if self.path == "/":
            self.path = "/index.html"
        try:
            sendReply = False
            if self.path.endswith(".html"):
                mimetype = "text/html"
                sendReply = True
            if self.path.endswith(".jpg"):
                mimetype = "image/jpg"
                sendReply = True
            if self.path.endswith(".csv"):
                mimetype = "text/csv"
                sendReply = True
            if self.path.endswith(".png"):
                mimetype = "image/png"
                sendReply = True
            if self.path.endswith(".gif"):
                mimetype = "image/gif"
                sendReply = True
            if self.path.endswith(".js"):
                mimetype = "application/js"
                sendReply = True
            if self.path.endswith(".css"):
                mimetype = "text/css"
                sendReply = True
            if self.path.endswith(".gltf"):
                mimetype = "text/json"
                sendReply = True
            if self.path.endswith(".mp3"):
                mimetype = "audio/mpeg"
                sendReply = True
            if self.path.endswith(".bin"):
                mimetype = "application/octet-stream"
                sendReply = True
            if sendReply == True:
                f = open(curdir + sep + self.path)
                self.send_response(200)
                self.send_header('Content-type',mimetype)
                self.end_headers()
                self.wfile.write(f.read())
                f.close()
            return
        except IOError:
            self.send_error(404,'File Not Found: %s' % self.path)

    def do_POST(request):

        print request.path

        if request.path == '/upload':
            length = request.headers['content-length']
            body = request.rfile.read(int(length))

            request.send_response(200)
            request.wfile.write("success")

        else:

            length = request.headers['content-length']
            body = json.loads(request.rfile.read(int(length)))

            request.send_response(200)
            request.send_header('Content-type','application/json')
            request.end_headers()

            resp = {}

            if 'test' in request.headers:
                if 'test' in body:
                    resp['test'] = 'success'
                else:
                    resp['test'] = 'failed'
            else:
                resp['test'] = 'failed'

            request.wfile.write(json.dumps(resp))

    def do_PUT(request):
        # for file upload
        print request
        print request.rfile

        request.send_response(200)
        request.send_header('Content-type','application/json')
        request.end_headers()

        request.wfile.write('success')

def savefile(self,file):
    outpath = os.path.join("", file.filename)
    with open(outpath, 'wb') as fout:
        shutil.copyfileobj(file.file, fout, 100000)
        return True

class MyTCPServer(SocketServer.TCPServer):
    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        self.socket.bind(self.server_address)

if __name__ == '__main__':
    try:
        port = 5555
        while True:
            try:
                httpd = MyTCPServer(('',port), MyHandler)

                print 'Serving on port', port

                httpd.serve_forever()

            except SocketServer.socket.error as exc:
                if exc.args[0] != 48:
                    raise
                print 'Port', port, 'already in use'
                port += 1
            else:
                break
    except KeyboardInterrupt:
        pass
    httpd.shutdown()
    print "Server Stopped"
