
function inter_1(next,frame_rate){

  var renderer = new mainRender();
  renderer.frame_rate = frame_rate;

  var audio = new Audio('sounds/inter.mp3');
  audio.loop = true;
  audio.play();

  var background = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/constBack.png",39,512,512,256,256,true,1.0);
  var title = new sprite(renderer.midpoint.x,renderer.midpoint.y,0,"images/constructor.png",50,512,512,256,256,true,1.0);

  renderer.renderCollection = [background,title];
  renderer.inputCollection = [];

  function moveOn(){
    audio.pause();
    renderer.play = false;
  }

  setTimeout(moveOn,2500);
  renderer.scene(next);

}
